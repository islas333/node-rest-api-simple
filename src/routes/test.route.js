import express from "express";
const router = express.Router();
const controller = require("../controllers/test.controller")
const path = "test";

router.get(`/${path}`, controller.getTest)

module.exports = router