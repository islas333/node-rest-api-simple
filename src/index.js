import sslServer from "./app"

const PORT = process.env.PORT || 4200;
sslServer.listen(PORT, () => {
  // console.log("server up and running "+`${PORT}`);
  console.log(`Server running at https://:${PORT}/`);
});
